module BasicConstraintsTest

using Test: @test, @test_throws, @testset

using ValueConstraints:
    Always,
    ConstraintViolation,
    ExclusiveMaximum,
    ExclusiveMinimum,
    In,
    Is,
    IsA,
    Maximum,
    Minimum,
    MultipleOf,
    Never,
    Not
using ValueConstraints.BasicConstraints: BINARY_CONSTRAINTS

@testset "basic constraints" begin
    @testset "have descriptive error messages" begin
        expected_messages = zip(
            # Defines the 'type' of the constraint
            first.(BINARY_CONSTRAINTS),
            # Phrases the relation between the arguments to the constraint
            last.(BINARY_CONSTRAINTS),
        )

        # These are arbitrary values that may not make sense to actually compare.
        # They are merely picked to test what the different error messages look
        # like
        actual_value = true
        constrained_value = false

        @testset "for $constraint()" for (constraint, error_content) in expected_messages
            violation = ConstraintViolation(
                eval(constraint)(constrained_value), actual_value
            )

            expected_error = "ConstraintViolation: $(actual_value) should be $(error_content) $(constrained_value)."

            @test expected_error == sprint(showerror, violation)
        end
    end

    @testset "validation" begin
        cases = (
            (Always(), 1234, true),
            (Always(), "foobar", true),
            (ExclusiveMaximum(5), 4, true),
            (ExclusiveMaximum(3), 3, false),
            (ExclusiveMaximum(2), 3, false),
            (ExclusiveMaximum('j'), 'i', true),
            (ExclusiveMaximum('j'), 'j', false),
            (ExclusiveMaximum('j'), 'k', false),
            (ExclusiveMinimum(2), 3, true),
            (ExclusiveMinimum(3), 3, false),
            (ExclusiveMinimum(5), 3, false),
            (ExclusiveMinimum('j'), 'i', false),
            (ExclusiveMinimum('j'), 'j', false),
            (ExclusiveMinimum('j'), 'k', true),
            (In([4321, 1234]), 1234, true),
            (In(1000:1500), 1234, true),
            (In([]), 1234, false),
            (In(), 1234, false),
            (In("foo", "bar"), "foo", true),
            (In("foo", "bar"), "quuz", false),
            (Is(1234), 1234, true),
            (Is(4321), 1234, false),
            (IsA(Int64), 1234, true),
            (IsA(Int64), "foo", false),
            (IsA(String), "foo", true),
            (IsA(String), 1234, false),
            (Maximum(5), 4, true),
            (Maximum(3), 3, true),
            (Maximum(2), 3, false),
            (Maximum('j'), 'i', true),
            (Maximum('j'), 'j', true),
            (Maximum('j'), 'k', false),
            (Minimum(2), 3, true),
            (Minimum(3), 3, true),
            (Minimum(5), 3, false),
            (Minimum('j'), 'i', false),
            (Minimum('j'), 'j', true),
            (Minimum('j'), 'k', true),
            (MultipleOf(2), 4, true),
            (MultipleOf(3), 9, true),
            (MultipleOf(5), 6, false),
            (Never(), 1234, false),
            (Never(), "foobar", false),
            (Not(Always()), 1234, false),
            (Not(Never()), 1234, true),
            (Not(Maximum(5)), 4, false),
            (Not(Maximum(2)), 3, true),
        )

        @testset "$constraint should $(result ? "" : "not ")be valid for `$value`" for (
            constraint, value, result
        ) in cases
            @test isvalid(constraint, value) === result
        end

        @testset "can optionally trigger exception" begin
            @testset "if explicitly specified to" begin
                constraint = Never()
                value = 1234

                @test_throws ConstraintViolation(constraint, value) isvalid(
                    constraint, value; fatal=true
                )
            end

            @testset "unless explicitly specified not to" begin
                @test isvalid(Never(), 1; fatal=false) === false
            end
        end
    end
end

end
