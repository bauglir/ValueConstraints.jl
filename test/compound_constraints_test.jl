module CompoundConstraintsTest

using Test: @test, @testset

using ValueConstraints:
    All,
    Always,
    AtLeast,
    AtMost,
    ConstraintViolation,
    Exactly,
    Maximum,
    Minimum,
    Never,
    None

@testset "compound constraints" begin
    test_value = true

    @testset "`All`" begin
        @testset "is valid if all sub-`constraints` are valid" begin
            @test isvalid(All(Always(), Always(), Always()), test_value)
        end

        @testset "is invalid if at least one of the sub-`constraints` is invalid" begin
            @test !isvalid(All(Always(), Never(), Always()), test_value)
        end

        @testset "is valid if there are no sub-`constraints`" begin
            @test isvalid(All(), test_value)
        end
    end

    @testset "`AtLeast`" begin
        @testset "is valid if at least the specified amount of sub-`constraints` are valid" begin
            @test isvalid(AtLeast{2}(Always(), Always(), Never()), test_value)
        end

        @testset "is invalid if less than the specified number of sub-`constraints` are valid" begin
            @test !isvalid(AtLeast{3}(Always(), Never(), Always()), test_value)
        end

        @testset "can be constructed using function arguments" begin
            @test AtLeast(1, Never(), Always()) === AtLeast{1}(Never(), Always())
            @test AtLeast(2) === AtLeast{2}()
        end

        @testset "simplifies to base constraints for known edge cases" begin
            @test AtLeast{0}() === Always()
            @test AtLeast{2}() === Never()
        end

        @testset "has a descriptive error message" begin
            # For this constraint to pass, at least 3 out of 4 constraints, i.e. 2
            # more constraints of the 3 violated constraints, have to pass
            actual_value = 1
            maximum_constraint = Maximum(0)
            minimum_constraint = Minimum(3)
            constraint = AtLeast{3}(
                maximum_constraint, Always(), Never(), minimum_constraint
            )

            maximum_violation = ConstraintViolation(maximum_constraint, actual_value)
            maximum_violation_message = sprint(print, maximum_violation)
            minimum_violation = ConstraintViolation(minimum_constraint, actual_value)
            minimum_violation_message = sprint(print, minimum_violation)
            never_violation = ConstraintViolation(Never(), actual_value)
            never_violation_message = sprint(print, never_violation)
            violation = ConstraintViolation(constraint, actual_value)

            expected_error = """
            ConstraintViolation: the provided value has to satisfy at least 2 out of the following 3 violated constraints:
              - $maximum_violation_message
              - $never_violation_message
              - $minimum_violation_message
            """

            @test expected_error == sprint(showerror, violation)
        end
    end

    @testset "`AtMost`" begin
        @testset "is valid if at most the specified amount of sub-`constraints` are valid" begin
            @test isvalid(AtMost{2}(Always(), Always(), Never()), test_value)
            @test isvalid(AtMost{2}(Always(), Never(), Never()), test_value)
            @test isvalid(AtMost{2}(Never(), Never(), Never()), test_value)
        end

        @testset "is invalid if more than the specified number of sub-`constraints` are valid" begin
            @test !isvalid(AtMost{1}(Always(), Always(), Always()), test_value)
            @test !isvalid(AtMost{1}(Always(), Always(), Never()), test_value)
        end

        @testset "can be constructed using function arguments" begin
            @test AtMost(1, Never(), Always()) === AtMost{1}(Never(), Always())
            @test AtMost(3) === AtMost{3}()
        end

        @testset "simplifies to base constraints for known edge cases" begin
            @test AtMost{0}() === Always()
            @test AtMost{3}() === Always()
        end

        @testset "has a descriptive error message" begin
            # For this constraint to pass, at most 1 out of 3 constraints, i.e. 1
            # less constraint of the 2 passing constraints, have to pass
            actual_value = 1
            maximum_constraint = Maximum(3)
            constraint = AtMost{1}(Always(), Never(), maximum_constraint)

            always_violation = ConstraintViolation(Always(), actual_value)
            always_violation_message = sprint(print, always_violation)
            maximum_violation = ConstraintViolation(maximum_constraint, actual_value)
            maximum_violation_message = sprint(print, maximum_violation)
            violation = ConstraintViolation(constraint, actual_value)

            expected_error = """
            ConstraintViolation: the provided value has to satisfy at most 1 out of the following 2 passing constraints:
              - $always_violation_message
              - $maximum_violation_message
            """

            @test expected_error == sprint(showerror, violation)
        end
    end

    @testset "`Exactly`" begin
        @testset "is valid if exactly the specified amount of sub-`constraints` are valid" begin
            @test isvalid(Exactly{0}(Never(), Never()), test_value)
            @test isvalid(Exactly{1}(Always(), Never()), test_value)
            @test isvalid(Exactly{2}(Always(), Never(), Always()), test_value)
        end

        @testset "is invalid if not exactly the specified number of sub-`constraints` are valid" begin
            @test !isvalid(Exactly{0}(Always(), Never()), test_value)
            @test !isvalid(Exactly{1}(Always(), Always()), test_value)
            @test !isvalid(Exactly{2}(Always(), Never(), Never()), test_value)
        end

        @testset "can be constructed using function arguments" begin
            @test Exactly(0) === Exactly{0}()
            @test Exactly(3) === Exactly{3}()

            @test Exactly(0, Never()) === Exactly{0}(Never())
        end

        @testset "simplifies to base constraints for known edge cases" begin
            @test Exactly{0}() === Always()
            @test Exactly{3}() === Never()
        end

        @testset "has a descriptive error message" begin
            actual_value = 1
            maximum_constraint = Maximum(0)
            constraint = All(Always(), Never(), maximum_constraint)

            maximum_violation = ConstraintViolation(maximum_constraint, actual_value)
            maximum_violation_message = sprint(print, maximum_violation)
            never_violation = ConstraintViolation(Never(), actual_value)
            never_violation_message = sprint(print, never_violation)
            violation = ConstraintViolation(constraint, actual_value)

            expected_error = """
            ConstraintViolation: the provided value has to satisfy all of the following constraints:
              - $never_violation_message
              - $maximum_violation_message
            """

            @test expected_error == sprint(showerror, violation)
        end
    end

    @testset "`None`" begin
        @testset "is valid if all sub-`constraints` are invalid" begin
            @test isvalid(None(Never(), Never(), Never()), test_value)
        end

        @testset "is invalid if at least one of the sub-`constraints` is valid" begin
            @test !isvalid(None(Always(), Never(), Never()), test_value)
        end

        @testset "is valid if there are no sub-`constraints`" begin
            @test isvalid(None(), test_value)
        end
    end
end

end
