module ValidationTest

using Test: @test, @test_throws, @testset

using ValueConstraints: Always, ConstraintViolation, Never

@testset "validation" begin
    @testset "can be deferred" begin
        validator = isvalid(Always())

        @test validator(5)
        @test validator("foo")

        @testset "which can generate `ConstraintViolation`s" begin
            constraint = Never()
            value = "bar"

            @testset "when instructed to" begin
                fatal_validator = isvalid(constraint; fatal=true)

                @test_throws ConstraintViolation(constraint, value) fatal_validator(value)
            end

            @testset "unless instructed not to" begin
                non_fatal_validator = isvalid(constraint; fatal=false)

                @test non_fatal_validator(value) === false
            end
        end
    end
end

end
