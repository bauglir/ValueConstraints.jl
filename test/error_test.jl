module ErrorTest

using Test: @test, @test_throws, @testset

using ValueConstraints: ConstraintViolation, Never

@testset "errors" begin
    @testset "can be thrown using `Base.error`" begin
        constraint = Never()
        value = 1234

        @test_throws ConstraintViolation(constraint, value) error(constraint, value)
    end

    @testset "show a descriptive message" begin
        constraint = Never()
        value = 4321

        error_message = sprint(showerror, ConstraintViolation(constraint, value))

        @test contains(error_message, "ConstraintViolation")
        @test contains(error_message, "$(constraint)")
        @test contains(error_message, "does not validate against")
        @test contains(error_message, "$(value)")
    end
end

end
