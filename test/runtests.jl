module ValueConstraintsTest

using Test: @test, @testset

using ValueConstraints

@testset "ValueConstraints" begin
    include.(
        joinpath.(
            @__DIR__,
            (
                "basic_constraints_test.jl",
                "compound_constraints_test.jl",
                "constrained_test.jl",
                "error_test.jl",
                "validation_test.jl",
            ),
        )
    )
end

end
