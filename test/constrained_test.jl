module ConstrainedTest

using Test: @test, @test_throws, @testset

using ValueConstraints

@testset "`Constrained`" begin
    @testset "construction" begin
        @testset "ensures validity by default" begin
            @test_throws(ConstraintViolation, Constrained(11, Maximum(10)))
        end

        @testset "validation is optional" begin
            expected_value = 5
            constraint = Constrained(expected_value, Minimum(10); fatal=false)
            @test constraint.value === expected_value
        end
    end

    @testset "constraint cannot be modified after construction" begin
        constrained = Constrained(100, Maximum(1000))

        @test_throws(ErrorException, constrained.constraint = Maximum(100))
    end

    @testset "can be queried for its validity" begin
        valid_constrained = Constrained("foo", In("foo", "bar"); fatal=false)
        @test isvalid(valid_constrained)

        invalid_constrained = Constrained("quuz", In("foo", "bar"); fatal=false)
        @test !isvalid(invalid_constrained)

        @testset "validation accepts `fatal` override" begin
            @test_throws(ConstraintViolation, isvalid(invalid_constrained; fatal=true))
        end
    end

    @testset "can be queried for its `value`" begin
        expected_value = 123
        constrained = Constrained(expected_value, IsA(Int))
        @test constrained.value === expected_value
    end

    @testset "(optionally) ensures validity when updating its `value`" begin
        invalid_value = 789

        constrained = Constrained(456, In(123, 456))

        @test_throws(ConstraintViolation, constrained.value = invalid_value)

        constrained.fatal = false
        constrained.value = invalid_value

        @test !isvalid(constrained)
    end

    @testset "supports dereferencing" begin
        @testset "for queries" begin
            expected_value = 123

            constrained = Constrained(expected_value, Is(expected_value))

            @test constrained[] === expected_value
        end

        @testset "for assignment" begin
            expected_value = 456

            constrained = Constrained(123, In(123, 456))

            constrained[] = expected_value
            @test constrained.value === expected_value

            @testset "(optionally) triggers immediate validation" begin
                @test_throws(ConstraintViolation, constrained[] = 789)

                constrained.fatal = false
                constrained[] = 789

                @test !isvalid(constrained)
            end
        end
    end
end

end
