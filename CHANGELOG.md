# Changelog

# [0.1.0](https://gitlab.com/bauglir/ValueConstraints.jl/compare/v0.0.1...v0.1.0) (2023-07-28)


### Features

* add 'compound constraint' constructors ([2ccb11e](https://gitlab.com/bauglir/ValueConstraints.jl/commit/2ccb11eccc85379a79e0777f6a252e335dbc0f63))
* add `Always` constraint that is valid for any value ([cdb4a17](https://gitlab.com/bauglir/ValueConstraints.jl/commit/cdb4a171ac2c61339f833cc6c1623c56acc0029d))
* add `Constrained` for constraining (primitive) values ([987b81d](https://gitlab.com/bauglir/ValueConstraints.jl/commit/987b81dd498f3633b78c67a5f33e2a92a6061653))
* add `Never` constraint that is invalid for any value ([a45500d](https://gitlab.com/bauglir/ValueConstraints.jl/commit/a45500de47999e7af0b29716098b45cadd0d32b3))
* add `Not` constraint to invert any existing constraint ([bb61297](https://gitlab.com/bauglir/ValueConstraints.jl/commit/bb612979b6527ab8928987a88d68f177a3cd73f5))
* add common constraints for instances of types ([88d3ea2](https://gitlab.com/bauglir/ValueConstraints.jl/commit/88d3ea20cedcaedf00e776f4008485c73a9942e3))
* add common constraints for numbers ([4664c05](https://gitlab.com/bauglir/ValueConstraints.jl/commit/4664c0575b1aaabdef4c5fe079cdfe3ab75ac0b6))
* **basic_constraints:** add multi-argument constructor for `In` ([a48f5fc](https://gitlab.com/bauglir/ValueConstraints.jl/commit/a48f5fc96d9ca7f054e995f8ab1fc70d92063bd5))
* define and render `ConstraintViolation`s ([06eb3b8](https://gitlab.com/bauglir/ValueConstraints.jl/commit/06eb3b8f047619d3bf9318e703e34384dff1a96e))
* enable constraint validation with integrated error generation ([86f5711](https://gitlab.com/bauglir/ValueConstraints.jl/commit/86f57112eb56e7a064a81c17b7327619dd4f3601))
* enable the creation of deferred validators ([3e9c985](https://gitlab.com/bauglir/ValueConstraints.jl/commit/3e9c985f7871fc8b335af6ec0c94e9f4f8580964))
* facilitate creation of custom constraints ([b191efa](https://gitlab.com/bauglir/ValueConstraints.jl/commit/b191efabb35022432c12c2d76ab0c7bd8f8a7fb8))
* print friendly error messages for 'compound' `ConstraintViolation`s ([7791e12](https://gitlab.com/bauglir/ValueConstraints.jl/commit/7791e12dbdd97b746cd1816198af09cace09dd8e))
* print friendly error messages for `ConstraintViolation`s ([c2b869e](https://gitlab.com/bauglir/ValueConstraints.jl/commit/c2b869eeba1b7dbcffbfdbb42a7d8dff77d5be5f))


### Performance Improvements

* create simple constraints as parametric functions ([9686843](https://gitlab.com/bauglir/ValueConstraints.jl/commit/9686843e03dac57cf47730dacf91572b83a0fcdf))
* tighten typing of `CompoundConstraint`s ([9d9a9d6](https://gitlab.com/bauglir/ValueConstraints.jl/commit/9d9a9d6ae70d3a2f59d3c7c2455135941eea29e0))
