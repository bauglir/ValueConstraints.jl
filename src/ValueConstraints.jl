"""
$(README)
"""
module ValueConstraints

"Helper macro for consistent formatting of docstrings across modules."
macro formatDocstrings()
    # This needs to be in two separate `eval`s to ensure the functionality
    # `export`ed by `DocStringExtensions` is available
    __module__.eval(:(using DocStringExtensions))

    return __module__.eval(
        quote
            @template DEFAULT = """
                                $(TYPEDSIGNATURES)
                                $(DOCSTRING)
                                """

            @template (TYPES) = """
                                $(TYPEDEF)
                                $(TYPEDFIELDS)
                                $(DOCSTRING)
                                """
        end,
    )
end

@formatDocstrings

using Reexport: @reexport

function loadSubmodule(file::String)
    submodule = include(joinpath(@__DIR__, "value_constraints", "$(file).jl"))
    return eval(:(@reexport using .$(nameof(submodule))))
end

loadSubmodule.([
    "interface",
    "error_handling",
    "basic_constraints",
    "compound_constraints",
    "constrained",
])

end
