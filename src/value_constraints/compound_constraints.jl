"""
Contains all functionality related to 'compound constraints', i.e. constraints
consisting of one or more 'sub-constraints' bound by a higher level constraint
on the result of those 'sub-constraints', e.g. 'all of these constraints',
'some of these constraints', etc.
"""
module CompoundConstraints

using ..BasicConstraints: Always, Never
using ..ErrorHandling: ConstraintViolation
using ..Interface: AbstractConstraint
using ..ValueConstraints: @formatDocstrings
@formatDocstrings

export All, None

"""
Abstract representation of collections of constraints that define a restriction
on the aggregate.
"""
abstract type CompoundConstraint <: AbstractConstraint end

"""
Represents one or more [`AbstractConstraint`](@ref)s as wrapped by a
[`CompoundConstraint`](@ref).
"""
const OneOrMoreConstraints = Tuple{AbstractConstraint,Vararg{AbstractConstraint}}

const COMPOUND_CONSTRAINTS = (
    (:AtLeast, >=, "at least", "a sufficient amount of `constraints` has"),
    (:AtMost, <=, "at most", "too many `constraints` have"),
    (:Exactly, ==, "exactly", "too many `constraints` have"),
)

# Defines a `CompoundConstraint` based on the definitions in
# `COMPOUND_CONSTRAINTS` adding several constructors to provide an easier to
# use API
function defineCompoundConstraint(
    (
        name, constraint, requirement_statement, constraint_success_statement
    )::Tuple{Symbol,Function,String,String},
)
    docstring = """
    A compound constraint requiring $requirement_statement `S` of its wrapped
    `constraints` to be valid.

    All wrapped `constraints` are 'eagerly' evaluated, i.e. even if
    $constraint_success_statement already succeeded all others are still
    evaluated as well.
    """

    eval(
        quote
            export $name

            @doc $docstring struct $name{S,T<:OneOrMoreConstraints} <: CompoundConstraint
                "The collection of `AbstractConstraint`s that is to be evaluated."
                constraints::T
            end
            function $name{S}(constraints::AbstractConstraint...) where {S}
                return $name{S,Tuple{typeof.(constraints)...}}(constraints)
            end
            function $name(amount_valid::Integer, constraints::AbstractConstraint...)
                return $name{amount_valid}(constraints...)
            end
            $name(amount_valid::Integer) = $name{amount_valid}()

            function (compound_constraint::$name{S,T})(
                value
            ) where {S,T<:OneOrMoreConstraints}
                valid_constraints = isvalid.(compound_constraint.constraints, value)
                return $constraint(sum(valid_constraints), S)
            end
        end,
    )
end

defineCompoundConstraint.(COMPOUND_CONSTRAINTS)

# Although most logic to render error messages for `ConstraintViolation`s for
# `CompoundConstraint`s is generic and implemented through a corresponding
# `Base.print` method, individual types of `CompoundConstraint`s require
# different headers to accompany the description of the failing
# sub-constraints. These headers are implemented through methods of
# `compoundConstraintViolationHeader`
function compoundConstraintViolationHeader(
    violation::ConstraintViolation{AtLeast{S,T}}, messages::Tuple{Vararg{String}}
) where {S,T<:OneOrMoreConstraints}
    n_violated = length(messages)
    n_required_to_pass = S - length(violation.constraint.constraints) + n_violated

    return string(
        "the provided value has to satisfy at least ",
        n_required_to_pass,
        " out of the following ",
        n_violated,
        " violated constraints",
    )
end

function compoundConstraintViolationHeader(
    ::ConstraintViolation{AtMost{S,T}}, messages::Tuple{Vararg{String}}
) where {S,T<:OneOrMoreConstraints}
    return string(
        "the provided value has to satisfy at most ",
        S,
        " out of the following ",
        length(messages),
        " passing constraints",
    )
end

function compoundConstraintViolationHeader(
    ::ConstraintViolation{<:Exactly}, ::Tuple{Vararg{String}}
)
    return "the provided value has to satisfy all of the following constraints"
end

# Most `CompoundConstraint`s need to print the invalid constraints to provide
# feedback on which ones need to be validated
function getViolatedConstraintsIndex(violation::ConstraintViolation{<:CompoundConstraint})
    return .!collect(isvalid.(violation.constraint.constraints, violation.value))
end

# The `AtMost` compound constraint needs to print the _valid_ constraints
# instead of the invalid ones, as when it fails one or more of the valid
# constraints needs to be invalidated
function getViolatedConstraintsIndex(violation::ConstraintViolation{<:AtMost})
    return collect(isvalid.(violation.constraint.constraints, violation.value))
end

# Generic function to print errors for `CompoundConstraint`s. Uses
# `getViolatedConstraintsIndex` to know which messages to print for violated
# (sub)constraints and `compoundConstraintViolationHeader` to print an
# appropriate header
function Base.print(io::IO, violation::ConstraintViolation{<:CompoundConstraint})
    constraint_violations =
        ConstraintViolation.(
            violation.constraint.constraints[getViolatedConstraintsIndex(violation)],
            violation.value,
        )

    violation_messages = sprint.(print, constraint_violations)

    return print(
        io,
        "$(compoundConstraintViolationHeader(violation, violation_messages)):\n",
        string.("  - ", violation_messages, "\n")...,
    )
end

# Some constraints, typically those without any sub-constraints, revert to very
# basic instances. These are defined explicitly to make the easier to evaluate
AtLeast{0}() = Always()
AtLeast{S}() where {S} = Never()
AtMost{S}() where {S} = Always()
Exactly{S}() where {S} = Never()
Exactly{0}() = Always()

"""
A compound constraint requiring all `constraints` to be valid.

This is a shorthand for instantiating an [`Exactly`](@ref) constraint for the
amount of passed `constraints`.

All wrapped constraints are 'eagerly' evaluated, i.e. even if one of the
`constraints` fails, all others are still evaluated as well.
"""
All(constraints::AbstractConstraint...) = Exactly{length(constraints)}(constraints...)
All() = Exactly{0}()

"""
A compound constraint requiring none of the `constraints` to be valid or,
phrased differently, requiring all of the `constraints` to be invalid.

This is a shorthand for instantiating an [`Exactly`](@ref) constraint requiring
`0` `constraints` to be valid.

All wrapped constraints are 'eagerly' evaluated, i.e. even if one of the
`constraints` succeeds, all others are still evaluated as well.
"""
None(constraints::AbstractConstraint...) = Exactly{0}(constraints...)
None() = Exactly{0}()

end
