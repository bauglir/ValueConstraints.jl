"Defines error handling for failing constraints."
module ErrorHandling

using ..Interface: AbstractConstraint
using ..ValueConstraints: @formatDocstrings
@formatDocstrings

export ConstraintViolation

"""
An `Exception` representing a `value` that failed validation against a
`constraint`.

To display descriptive error messages when a `ConstraintViolation` is generated
for a specific [`AbstractConstraint`](@ref), define a corresponding
`Base.print(io::IO, violation::ConstraintViolation{AbstractConstraint})`
method.

Describing errors for [`CompoundConstraint`](@ref)s requires special care, see
[`Base.print(io::IO,
violation::ConstraintViolation{<:CompoundConstraint})`](@ref) for details.
"""
struct ConstraintViolation{S<:AbstractConstraint} <: Exception
    "The `constraint` that was violated."
    constraint::S

    "The `value` that violated the constraint."
    value
end

"""
Throws a [`ConstraintViolation`](@ref) corresponding to the provided
`constraint` and `value`.
"""
function Base.error(constraint::AbstractConstraint, value)
    throw(ConstraintViolation(constraint, value))
end

"Renders a descriptive error for [`ConstraintViolation`](@ref)s."
function Base.showerror(io::IO, violation::ConstraintViolation)
    printstyled(io, "ConstraintViolation: "; color=:red)
    return print(io, violation)
end

"Renders a generic message for [`ConstraintViolation`](@ref)s."
function Base.print(io::IO, violation::ConstraintViolation)
    return print(
        io, "$(violation.value) does not validate against $(violation.constraint)."
    )
end

end
