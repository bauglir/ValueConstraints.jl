"Defines the basic interface for defining and validating constraints."
module Interface

using ..ValueConstraints: @formatDocstrings
@formatDocstrings

export AbstractConstraint

"Base representation for any kind of constraint on a value."
abstract type AbstractConstraint <: Function end

"""
The default value for the `fatal` keyword argument of `isvalid` for
[`AbstractConstraint`](@ref)s.
"""
const DEFAULT_FATAL_KEYWORD_ARGUMENT = false
const DEFAULT_FATAL_DOCSTRING = "The `fatal` keyword argument defaults to `$(DEFAULT_FATAL_KEYWORD_ARGUMENT)`."

"""
Validates a `value` against a `constraint`.

Errors, i.e. throws a [`ConstraintViolation`](@ref), if `fatal` is set to
`true`.

$DEFAULT_FATAL_DOCSTRING
"""
function Base.isvalid(
    constraint::AbstractConstraint, value; fatal::Bool=DEFAULT_FATAL_KEYWORD_ARGUMENT
)
    return constraint(value) || (fatal && error(constraint, value))
end

"""
Creates a validator for a `constraint` that can be evaluated at a later time.

The validator will error, i.e. throws a [`ConstraintViolation`](@ref), upon
failed validation if `fatal` is set to `true`.

$DEFAULT_FATAL_DOCSTRING
"""
function Base.isvalid(
    constraint::AbstractConstraint; fatal::Bool=DEFAULT_FATAL_KEYWORD_ARGUMENT
)
    return value -> isvalid(constraint, value; fatal=fatal)
end

end
