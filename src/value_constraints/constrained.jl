module ConstrainedValues

using ..Interface: AbstractConstraint

export Constrained

"""
A container to enforce an [`AbstractConstraint`](@ref) on a (primitive) value,
which may change over time.

The `value` is, optionally controlled through the value of `fatal`, validated
upon initial construction, as well as whenever it is mutated.
"""
mutable struct Constrained{C,V}
    "The [`AbstractConstraint`](@ref) to apply to the `value`."
    const constraint::C

    "Whether a validation failure of the `constraint` is considered an error."
    fatal::Bool

    "The (mutable) value to constrain according to the `constraint`."
    value::V

    function Constrained(
        value::V, constraint::C; fatal=true
    ) where {C<:AbstractConstraint,V}
        isvalid(constraint, value; fatal)
        return new{C,V}(constraint, fatal, value)
    end
end

function Base.getindex((; value)::Constrained)
    return value
end

function Base.isvalid(constrained::Constrained; fatal::Bool=constrained.fatal)
    return isvalid(constrained.constraint, constrained.value; fatal)
end

function Base.setindex!(
    constrained::Constrained{C,V}, value::V
) where {C<:AbstractConstraint,V}
    return constrained.value = value
end

function Base.setproperty!(
    constrained::Constrained{C,V}, property::Symbol, value::V
) where {C<:AbstractConstraint,V}
    property === :value && isvalid(constrained.constraint, value; fatal=constrained.fatal)

    return setfield!(constrained, property, value)
end

end
