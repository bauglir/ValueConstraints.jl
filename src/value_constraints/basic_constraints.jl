"""
Provides a number of low-level 'basic' constraints for different types of
values, e.g. 'must be at least', 'must be less than', 'must have this format',
etc. and accompanying logic, such as the definition of user-friendly error
messages.
"""
module BasicConstraints

using ..ErrorHandling: ConstraintViolation
using ..Interface: AbstractConstraint
using ..ValueConstraints: @formatDocstrings

@formatDocstrings

# Export the statically defined `AbstractConstraint`s, all other `export`s are
# declared upon definition
export Always, Never, Not

"""
Represents a constraint that is valid no matter which `value` is validated
against it.

Useful for shortcircuiting code, e.g. in tests, that requires validation of a
constraint where it is not straightforward to make less trivial constraints
pass.
"""
struct Always <: AbstractConstraint end
(::Always)(::Any) = true

"""
Represents a constraint that is _not_ valid no matter which `value` is
validated against it.
"""
struct Never <: AbstractConstraint end
(::Never)(::Any) = false

"Represents the inversion of a `constraint`."
struct Not{S<:AbstractConstraint} <: AbstractConstraint
    "The `constraint` to invert."
    constraint::S
end
(not::Not)(value) = !not.constraint(value)

# Defines the necessary types and relevant methods, e.g. `isvalid`, `print`,
# etc. for 'binary' constraints, i.e. constraints consisting of 2 arguments
# typically being the value to validate and the value to validate it against
function defineBinaryConstraint(
    (name, validator, docstring, error_description)::Tuple{Symbol,Function,String,String}
)
    eval(
        quote
            export $name

            @doc $docstring struct $name{S} <: AbstractConstraint
                "The `value` to validate against."
                value::S
            end
            (constraint::$name)(value) = $validator(value, constraint.value)

            # Renders a friendly message for '$name' `ConstraintViolation`s. Enables
            # composable error messages for compound constraints
            function Base.print(io::IO, violation::ConstraintViolation{$name{S}}) where {S}
                error_components = [
                    violation.value,
                    "should be",
                    $error_description,
                    violation.constraint.value,
                    ".",
                ]

                return join(io, error_components, " ", "")
            end
        end,
    )
end

const BINARY_CONSTRAINTS = (
    (:In, in, "Constrains a value to be in a set of values", "a member of"),
    (:Is, ===, "Constrains a value to be some value.", "equal to"),
    (:IsA, isa, "Constrains a value to be of a specific type.", "a"),
    (
        :ExclusiveMaximum,
        <,
        "Constrains a value to be less than a specified value.",
        "less than",
    ),
    (
        :ExclusiveMinimum,
        >,
        "Constrains a value to be larger than a specified value.",
        "larger than",
    ),
    (
        :Maximum,
        <=,
        "Constrains a value to be less than or equal to a specified value.",
        "less than or equal to",
    ),
    (
        :Minimum,
        >=,
        "Constrains a value to be larger than or equal to a specified value.",
        "larger than or equal to",
    ),
    (
        :MultipleOf,
        (value, constraint_value) -> (rem(value, constraint_value) === 0),
        "Constrains a value to be a multiple of a specified value.",
        "an exact multiple of",
    ),
)

defineBinaryConstraint.(BINARY_CONSTRAINTS)

# Convenience methods of constraint constructors
In(args...) = In(args)

end
